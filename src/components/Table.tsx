import { SkMatrix, Group, Path } from "@shopify/react-native-skia";
import type { SharedValue } from "react-native-reanimated";

interface Point {
  x: number;
  y: number;
}

export interface TableProps {
  matrix: SkMatrix | SharedValue<SkMatrix>;
}

export const SIZE = 100;

function isContained(pointer: Point | null, position: Point) {
  "worklet";
  if (pointer === null) {
    return false;
  }
  console.log({ pointer });

  return (
    pointer.x > position.x &&
    pointer.x < position.x + SIZE &&
    pointer.y > position.y &&
    pointer.y < position.y + SIZE
  );
}

const Table: React.FC<TableProps> = ({ matrix }) => {
  return (
    <Group matrix={matrix}>
      <Path
        path="M5 0C2.5 0 0 2.5 0 5V95C0 100 2.5 100 5 100H95C97.5 100 100 97.5 100 95V5C100 2.5 97.5 0 95 0H5Z"
        color="#c7f8ff"
      />
    </Group>
  );
};

export default Table;
