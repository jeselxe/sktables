import React from "react";
import { Button, Dimensions, StyleSheet, View } from "react-native";
import { useTableContext } from "../lib/TableContext";
import {
  Blur,
  Canvas,
  ColorMatrix,
  Group,
  Paint,
  fitbox,
  processTransform2d,
  rect,
} from "@shopify/react-native-skia";
import Table, { SIZE } from "../components/Table";
import { GestureHandler } from "../components/GestureHandler";
import { deflate } from "../lib/geometryHelpers";
import { makeMutable } from "react-native-reanimated";

interface TablesProps {}

const window = Dimensions.get("window");

const Tables = ({}: TablesProps) => {
  const { tables, addTable } = useTableContext();

  return (
    <View style={styles.container}>
      <Canvas style={styles.container}>
        <Group
          layer={
            <Paint>
              <Blur blur={12} />
              <ColorMatrix
                matrix={[
                  1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 20, -7,
                ]}
              />
            </Paint>
          }
        >
          {tables.map(({ matrix }, i) => (
            <Table key={i} matrix={matrix} />
          ))}
        </Group>
      </Canvas>
      {tables.map(({ size, matrix, name }, idx) => (
        <GestureHandler
          key={idx}
          matrix={matrix}
          size={size}
          name={name}
          debug
        />
      ))}
      <View style={styles.btn}>
        <Button
          title="Nueva Mesa"
          onPress={() => {
            const src = rect(0, 0, SIZE, SIZE);
            const dst = deflate(rect(0, 0, window.width, window.height), 84);
            const m3 = processTransform2d(fitbox("contain", src, dst));
            const matrix = makeMutable(m3);
            addTable({
              Table,
              size: {
                width: SIZE,
                height: SIZE,
              },
              name: `${tables.length + 1}`,
              matrix,
            });
          }}
        ></Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  btn: {
    position: "absolute",
    right: 10,
    bottom: 10,
  },
});

export default Tables;
