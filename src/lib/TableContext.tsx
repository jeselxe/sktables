import { type SkMatrix, type SkSize } from "@shopify/react-native-skia";
import { createContext, useCallback, useContext, useReducer } from "react";
import type { ReactNode, FC } from "react";
import type { SharedValue } from "react-native-reanimated";

import type { TableProps } from "../components/Table";

export interface Table {
  Table: FC<TableProps>;
  size: SkSize;
  matrix: SharedValue<SkMatrix>;
  name: string;
}

type Tables = Table[];

interface TableContext {
  tables: Tables;
  dispatch: (action: TableAction) => void;
}

const TableContext = createContext<TableContext | null>(null);

interface TableAction {
  action: "add";
  table: Table;
}

const tableReducer = (tables: Tables, action: TableAction) => {
  return [...tables, action.table];
};

export const useTableContext = () => {
  const ctx = useContext(TableContext);
  if (ctx === null) {
    throw new Error("No Table context found");
  }
  const { tables, dispatch } = ctx;
  const addTable = useCallback(
    (table: Table) => {
      dispatch({ action: "add", table });
    },
    [dispatch]
  );
  return {
    tables,
    addTable,
  };
};

interface TableProviderProps {
  children: ReactNode | ReactNode[];
}

export const TableProvider = ({ children }: TableProviderProps) => {
  const [tables, dispatch] = useReducer(tableReducer, []);
  return (
    <TableContext.Provider value={{ tables, dispatch }}>
      {children}
    </TableContext.Provider>
  );
};
