import {
  Canvas,
  Box,
  Fill,
  rrect,
  rect,
  BoxShadow,
  useTouchHandler,
  useValue,
  SkiaValue,
  useComputedValue,
  Group,
  Paint,
  Blur,
  ColorMatrix,
  processTransform2d,
  fitbox,
} from "@shopify/react-native-skia";
import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { Button, Dimensions, StyleSheet, Text, View } from "react-native";
import { TableProvider, useTableContext } from "./src/lib/TableContext";
import Table from "./src/components/Table";
import { GestureHandler } from "./src/components/GestureHandler";
import { deflate } from "./src/lib/matrixHelpers";
import { makeMutable } from "react-native-reanimated";
import Tables from "./src/screens/Tables";
import { GestureHandlerRootView } from "react-native-gesture-handler";

const SIZE = 100;
const GAP = 40;

// const TABLES_PER_ROW = Math.floor(width / (SIZE + GAP));
// console.log({ TABLES_PER_ROW });

// interface TableProps {
//   position: Point;
//   startPoint: SkiaValue<Point | null>;
//   pointer: SkiaValue<Point | null>;
// }

// function isContained(pointer: Point | null, position: Point) {
//   "worklet";
//   if (pointer === null) {
//     return false;
//   }
//   console.log({ pointer });

//   return (
//     pointer.x > position.x &&
//     pointer.x < position.x + SIZE &&
//     pointer.y > position.y &&
//     pointer.y < position.y + SIZE
//   );
// }

// const Table: React.FC<TableProps> = ({ pointer, position, startPoint }) => {
//   const contained = useComputedValue(() => {
//     const val = isContained(startPoint.current, position);
//     return val;
//   }, [startPoint]);
//   const color = useComputedValue(() => {
//     if (contained.current) {
//       return "#93b8c4";
//     } else {
//       return "#c7f8ff";
//     }
//   }, [contained]);
//   const box = useComputedValue(() => {
//     if (contained.current) {
//       return rrect(
//         rect(
//           pointer.current?.x ?? position.x,
//           pointer.current?.y ?? position.y,
//           SIZE,
//           SIZE
//         ),
//         24,
//         24
//       );
//     } else {
//       return rrect(rect(position.x, position.y, SIZE, SIZE), 24, 24);
//     }
//   }, [contained, pointer]);

//   return <Box box={box} color={color} />;
// };

// interface Point {
//   x: number;
//   y: number;
// }

export default function App() {
  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <TableProvider>
        <Tables />
      </TableProvider>
    </GestureHandlerRootView>
  );
}
